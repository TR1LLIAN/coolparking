﻿using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;

namespace CoolParkingConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            menu.ShowMenu();
        }
    }
}
