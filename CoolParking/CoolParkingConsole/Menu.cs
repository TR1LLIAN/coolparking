﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Linq;
using System.Text;

namespace CoolParkingConsole
{
    public class Menu
    {
        //Lengths of columns
        private int LenColTime = 18;
        private readonly int LenColId = 12;
        private readonly int LenColType = 14;
        private readonly int LenColNumber = 16;

        readonly string MainMenu = "Select menu item from 1 to 10 \n " +
            "1) Show Parking Balance \n " +
            "2) Amount of earned money for current period \n " +
            "3) Show quantity of free parking places \n " +
            "4) Show all transactions for current period of time \n " +
            "5) Show all transactions (Read Log) \n " +
            "6) Show all vehicles at parking \n " +
            "7) Place vehicle on to the parking \n " +
            "8) Remove vehicle from the parking \n " +
            "9) Top up balance of the vehicle \n" +
            "10) Exit";

        readonly ParkingService parkingService = new ParkingService();

        private void ShowBalance()
        {
            var temp = $"Parking balance is {parkingService.GetBalance()}";
            ShowBorder(temp);
        }

        private void AnswerConsole()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        void ShowBorder(string test)
        {
            Console.Clear();
            AnswerConsole();
            var temp = test;
            Console.WriteLine("╔" + new string('═', temp.Length) + "╗");
            Console.WriteLine("║" + temp + "║");
            Console.WriteLine("╚" + new string('═', temp.Length) + "╝");
            Console.ResetColor();
        }

        private void ShowFreePlace()
        {
            var temp = $"There is {parkingService.GetFreePlaces()} free places at the parking ";
            ShowBorder(temp);
        }

        private void EarnedMoneyPerPeriod()
        {
            var temp = $"Money earned for current period of time: {parkingService.GetLastParkingTransactions().Sum(tr => tr.Sum)} ";
            ShowBorder(temp);
        }

        private void ShowLastTransactions()
        {
            Console.Clear();
            AnswerConsole();
            StringBuilder a = new StringBuilder();
            var transactions = parkingService.GetLastParkingTransactions();
            int LenColBalance;
            int lenColSum;
            if (transactions.Length > 0)
            {
               LenColBalance = (transactions.Max(x => x.VehicleBalance).ToString().Length + 4) > 20 ? (transactions.Max(x => x.VehicleBalance).ToString().Length + 4) : 20 ;
               lenColSum = (transactions.Max(x => x.Sum).ToString().Length + 4) > 16 ? (transactions.Max(x => x.Sum).ToString().Length + 4) : 18;
               LenColTime = (transactions.Max(x => x.TransactionTime).ToString().Length + 4) > LenColTime ? (transactions.Max(x => x.TransactionTime).ToString().Length) : LenColTime;
            }
            else
            {
                lenColSum = 18;
                LenColBalance = 20;
            }
            int SymNum = lenColSum + LenColBalance + LenColTime + LenColId + 4;
            Console.WriteLine("╔" + new string('═', SymNum) + "╗");

            a.AppendLine("║" + new string(' ', (SymNum - 10) / 2) + " Vehicles " + new string(' ', (SymNum - 10 - (SymNum - 10) / 2)) + "║");
            a.AppendLine("╔" + new string('═', SymNum) + "╗");
            a.AppendLine("║" + new string(' ', (LenColTime - 17) / 2) + " Transaction time " + new string(' ', (LenColTime - 17 -(LenColTime - 18) / 2))
                + "║" + " Vehicle ID " + "║"
                + new string(' ', (LenColBalance - 17) / 2) + " Vehicle balance " + new string(' ', (LenColBalance - 17 - (LenColBalance - 17) / 2))
                + "║" + new string(' ', (lenColSum - 16) / 2 )
                + " With draw sum " + new string(' ', (lenColSum - 16 - (lenColSum - 16) / 2)) + "║");
            a.AppendLine("╚" + new string('═', SymNum) + "╝");

            if (transactions.Length != 0)
            {
                foreach (var tr in transactions)
                {
                    a.AppendLine("║" + new string(' ', (LenColTime - tr.TransactionTime.Length) / 2) + $" {tr.TransactionTime} " + new string(' ', LenColTime - tr.TransactionTime.Length - ( LenColTime - tr.TransactionTime.Length) / 2)
                  + "║" + $" {tr.VehicleId} " + "║"
                  + new string(' ', (LenColBalance - tr.VehicleBalance.ToString().Length-2) / 2) + $" {tr.VehicleBalance} " + new string(' ', (LenColBalance - (LenColBalance - tr.VehicleBalance.ToString().Length-2)/2 - tr.VehicleBalance.ToString().Length-2))+ "║"
                  + new string(' ', (lenColSum - tr.Sum.ToString().Length- 2) / 2) + $" {tr.Sum} " + new string(' ', (lenColSum  - tr.Sum.ToString().Length - 3 - (lenColSum - tr.Sum.ToString().Length-2)/2)) + "║");
                }
            }
            else
            {
                a.AppendLine("║" + new string(' ', (SymNum - 36) / 2) + " There was not a single transaction!" + new string(' ', (SymNum - 36) / 2) + "║");
            }

            Console.Write(a);
            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            Console.ResetColor();
        }

        private void ShowListParkedVehicles()
        {
            Console.Clear();
            AnswerConsole();
            StringBuilder a = new StringBuilder();
            var vehicles = parkingService.GetParkedVehicles();
            int LenColBalance;
            if (vehicles.Length > 0)
            {
                LenColBalance = (vehicles.Max(x => x.Balance).ToString().Length + 4) > 20 ? (vehicles.Max(x => x.Balance).ToString().Length + 4) : 20;
            }
            else
            {
                LenColBalance = 20;
            }
            int SymNum = LenColId + LenColBalance + LenColType + LenColNumber + 2;
            a.AppendLine("║" + new string(' ', (SymNum - 46) / 2) + "Last transactions for current period of time: " + new string(' ', (SymNum - 46) / 2 ) + "║");
            a.AppendLine('║' + new string('=', SymNum) + '║');
            a.AppendLine("╔" + new string('═', SymNum) + "╗");
            a.AppendLine("║" + " Vehicle number "
                + "║" + " Vehicle ID " + "║"
                + new string(' ', (LenColBalance - 17) / 2) + " Vehicle balance " + new string(' ', (LenColBalance - 17) / 2)
                + "║" + " Vehicle type " + "║");
            a.AppendLine("╚" + new string('═', SymNum) + "╝");

            Console.WriteLine("╔" + new string('═', SymNum) + "╗");
            if (vehicles.Length != 0)
            {
                int counter = 0;
                foreach (var Vh in vehicles)
                {
                    a.AppendLine(
                        "║" + new string(' ', ((LenColNumber - (counter + 1).ToString().Length) / 2)) + $" {counter++ + 1} " + new string(' ', (LenColNumber - (LenColNumber - ((counter + 1).ToString().Length)) / 2 - (counter + 1).ToString().Length-2))
                    + "║" + $" {Vh.Id} " + "║"
                    + new string(' ', (LenColBalance - Vh.Balance.ToString().Length - 2) / 2) + $" {Vh.Balance} " + new string(' ', (LenColBalance - (LenColBalance - Vh.Balance.ToString().Length - 2) / 2 - Vh.Balance.ToString().Length - 3)) + "║"
                    + new string(' ', (LenColType - Vh.VehicleType.ToString().Length - 2) / 2) + $" {Vh.VehicleType} " + new string(' ', (LenColType - Vh.VehicleType.ToString().Length - 2 - (LenColType - Vh.VehicleType.ToString().Length - 2) / 2)) + "║");
                }
            }
            else
            {
                a.AppendLine("║" + new string(' ', (SymNum - 47) / 2) + " There is not a single vehicle at the parking!" + new string(' ', (SymNum - 47) / 2) + " ║");
            }

            Console.Write(a);
            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            Console.ResetColor();
        }

        private void ShowParkedVehicles()
        {
           
            Console.Clear();
            AnswerConsole();
            StringBuilder a = new StringBuilder();
            var vehicles = parkingService.GetParkedVehicles();
            int LenColBalance;
            if (vehicles.Length > 0)
            {
                LenColBalance = (vehicles.Max(x => x.Balance).ToString().Length + 4) > 20 ? (vehicles.Max(x => x.Balance).ToString().Length + 4) : 20;
            }
            else
            {
                LenColBalance = 20;
            }
            int SymNum = LenColBalance + LenColId + LenColType + 1;
            a.AppendLine("║" + new string(' ', ((SymNum - 16) / 2)) + "Parked vehicles:" + new string(' ', (SymNum - 16 - ((SymNum - 16) / 2))) + "║");
            a.AppendLine('║' + new string('=', SymNum) + '║');
            a.AppendLine("╔" + new string('═', SymNum) + "╗");
            a.AppendLine("║" + " Vehicle ID " + "║"
                + new string(' ', (LenColBalance - 17) / 2) + " Vehicle balance " + new string(' ', (LenColBalance - 17) / 2)
                + "║" + " Vehicle type " + "║");
            a.AppendLine("╚" + new string('═', SymNum) + "╝");
            Console.WriteLine("╔" + new string('═', SymNum) + "╗");
            if (vehicles.Length != 0)
            {
                foreach (var Vh in vehicles)
                {
                    a.AppendLine( "║" + $" {Vh.Id} " + "║"
                    + new string(' ', (LenColBalance - Vh.Balance.ToString().Length - 2) / 2) + $" {Vh.Balance} " + new string(' ', (LenColBalance - (LenColBalance - Vh.Balance.ToString().Length - 2) / 2 - Vh.Balance.ToString().Length - 3)) + "║"
                    + new string(' ', (LenColType - Vh.VehicleType.ToString().Length - 2) / 2) + $" {Vh.VehicleType} " + new string(' ', (LenColType - Vh.VehicleType.ToString().Length - 2 - (LenColType - Vh.VehicleType.ToString().Length - 2) / 2)) + "║");
                }
            }
            else
            {
                a.AppendLine("║" + new string(' ', (SymNum - 46) / 2) + " There is not a single vehicle at the parking!" + new string(' ', (SymNum - 46) / 2) + " ║");
            }

            Console.Write(a);
            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            Console.ResetColor();
        }

        public void ReadLog()
        {
            int SymNum = 106;
            Console.Clear();
            AnswerConsole();
            var temp = parkingService.ReadFromLog();
            Console.WriteLine("╔" + new string('═', SymNum) + "╗");
            Console.WriteLine("║" + new string(' ', (SymNum - 3) / 2) + "Log:" + new string(' ', (SymNum - 3) / 2) + "║");
            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            if (temp == string.Empty)
            {
                Console.WriteLine("║" + new string(' ', (SymNum - 14) / 2) + "Log is empty! " + new string(' ', (SymNum - 14) / 2) + "║");
            }
            else
            {
                Console.WriteLine(temp);
            }

            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            Console.ResetColor();
        }

        private static void Error(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Error! {error}");
            Console.ResetColor();
        }

        private void AddVehicle()
        {
            Console.Clear();
            string ID;
            while (true)
            {
                Console.Write("Please enter vehicle ID(remember pattern (ХХ-YYYY-XX) X-letter Y-number:");
                try
                {
                    ID = Console.ReadLine();
                    if (Vehicle.CheckId(ID))
                    {
                        break;
                    }
                    else
                    {
                        throw new ArgumentException("ID don't match the pattern!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }


            string VehicleTypes =
                "1 - MotorCycle \n" +
                "2 - PessengerCar \n" +
                "3 - Bus \n" +
                "4 - Truck";
            VehicleType vehicleType = VehicleType.Motorcycle;
            int Type;
            while (true)
            {
                Console.WriteLine(VehicleTypes);
                Console.Write("Please select vehicle type: ");
                try
                {
                    Type = int.Parse(Console.ReadLine());
                    if (Type < 0 || Type > 4)
                    {
                        throw new ArgumentException("Type should be from 1 to 4!");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            switch (Type)
            {
                case 1:
                    vehicleType = VehicleType.Motorcycle;
                    break;
                case 2:
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case 3:
                    vehicleType = VehicleType.Bus;
                    break;
                case 4:
                    vehicleType = VehicleType.Truck;
                    break;

            }
            decimal balance;

            while (true)
            {
                Console.Write("Enter vehicle balance: ");
                try
                {
                    balance = decimal.Parse(Console.ReadLine());
                    if (balance <= 0)
                    {
                        throw new ArgumentException("Balance must be positive!");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            Vehicle vehicle = new(ID, vehicleType, balance);
            try
            {

                parkingService.AddVehicle(vehicle);
                Console.Clear();
                Console.ResetColor();
            }
            catch(Exception ex)
            {
                Error(ex.Message);
            }
        }

        private void RemoveVehicle()
        {
            Console.Clear();
            if (parkingService.GetVehicles().Count == 0)
            {
                AnswerConsole();
                Console.WriteLine("╔" + new string('═', 65) + "╗");
                Console.WriteLine("║ No Vehicles at the parking! So, you can not remove any vehicle! ║");
                Console.WriteLine("╚" + new string('═', 65) + "╝");
                Console.ResetColor();
                return;
            }
            else
            {
                ShowListParkedVehicles();
                int Number;
                while (true)
                {
                    Console.Write("Enter vehicle number to delete it(or enter 0 to exit to main menu): ");
                    try
                    {
                        Number = int.Parse(Console.ReadLine());
                        if (Number == 0)
                        {
                            Console.Clear();
                            return;
                        }
                        else if (Number > parkingService.GetVehicles().Count || Number < 0)
                        {
                            throw new ArgumentException("Wrong number!");
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Error(ex.Message);
                    }
                }
                var IdOfVehicle = parkingService.GetVehicles()[Number - 1].Id;
                try
                {
                    parkingService.RemoveVehicle(parkingService.GetVehicles()[Number - 1].Id);
                    Console.Clear();
                    AnswerConsole();
                    Console.WriteLine("╔" + new string('═', 59) + "╗");
                    Console.WriteLine($"║ Vehicle {IdOfVehicle} successfully removed from the parking! ║");
                    Console.WriteLine("╚" + new string('═', 59) + "╝");
                }
                catch(Exception ex)
                {
                    Error(ex.Message);
                }
        
                Console.ResetColor();
            }

        }

        private void TopUpVehicle()
        {
            Console.Clear();
            if (parkingService.GetVehicles().Count == 0)
            {
                AnswerConsole();
                Console.WriteLine("╔" + new string('═', 67) + "╗");
                Console.WriteLine("║ No Vehicles at the parking! So, you can not top up any vehicle! ║");
                Console.WriteLine("╚" + new string('═', 67) + "╝");
                Console.ResetColor();
                return;
            }
            else
            {
                ShowListParkedVehicles();
                int Number;
                while (true)
                {
                    Console.Write("Enter vehicle number to top up it: ");
                    try
                    {
                        Number = int.Parse(Console.ReadLine());
                        if (Number > parkingService.GetVehicles().Count || Number < 0)
                        {
                            throw new ArgumentException("Wrong number!");
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Error(ex.Message);
                    }
                }

                decimal TopUp;
                while (true)
                {
                    Console.Write("Enter amount of top up: ");
                    try
                    {
                        TopUp = decimal.Parse(Console.ReadLine());
                        if (TopUp <= 0)
                        {
                            throw new ArgumentException("Top up sum can not be negative!");
                        }
                        else
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Error(ex.Message);
                    }
                }

                try
                {
                    parkingService.TopUpVehicle(parkingService.GetVehicles()[Number - 1].Id, TopUp);
                    Console.Clear();
                    AnswerConsole();
                    Console.WriteLine("╔" + new string('═', 49) + "╗");
                    Console.WriteLine($"║ Vehicle {parkingService.GetVehicles()[Number - 1].Id} balance successfully top up! ║");
                    Console.WriteLine("╚" + new string('═', 49) + "╝");
                    Console.ResetColor();
                }
                catch(Exception ex)
                {
                    Error(ex.Message);
                }
            }
        }

        public void ShowMenu()
        {
            int Key = 0;
            Console.ResetColor();
            while (true)
            {
                Console.WriteLine(MainMenu);
                try
                {
                    Key = int.Parse(Console.ReadLine());
                    if (Key > 10 || Key < 1)
                    {
                        throw new ArgumentException("Wrong entry!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No such item! Please select menu item from 1 to 10!");
                    Console.ResetColor();
                }

                switch (Key)
                {
                    case 1:
                        ShowBalance();
                        break;
                    case 2:
                        EarnedMoneyPerPeriod();
                        break;
                    case 3:
                        ShowFreePlace();
                        break;
                    case 4:
                        ShowLastTransactions();
                        break;
                    case 5:
                        ReadLog();
                        break;
                    case 6:
                        ShowParkedVehicles();
                        break;
                    case 7:
                        AddVehicle();
                        break;
                    case 8:
                        RemoveVehicle();
                        break;
                    case 9:
                        TopUpVehicle();
                        break;
                    case 10: return;
                }
            }
        }
    }
}
