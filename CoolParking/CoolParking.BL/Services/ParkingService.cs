﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking parking;
        readonly List<TransactionInfo> transactionInfos;
        readonly int Capacity = Settings.Capacity;

        private readonly ILogService logService;
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.withdrawTimer = withdrawTimer;
            withdrawTimer.Elapsed += WithdrawTimer_Elapsed;

            this.logTimer = logTimer;
            logTimer.Elapsed += LogTimer_Elapsed;
            this.logService = logService;
            parking = Parking.GetInstance();
            transactionInfos = new List<TransactionInfo>();
        }

        public ParkingService()
        {
            parking = Parking.GetInstance();
            transactionInfos = new List<TransactionInfo>();

            logService = new LogService(Settings.PathFile);

            withdrawTimer = new TimerService(Settings.WithDrawInterval, WithdrawTimer_Elapsed);
            withdrawTimer.Start();

            logTimer = new TimerService(Settings.LogInterval, LogTimer_Elapsed);
            logTimer.Start();
        }

        public Vehicle[] GetParkedVehicles()
        {
            return parking.Vehicles.ToArray();
        }

        private void LogTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (transactionInfos.Count > 0)
            {

                foreach (var transaction in transactionInfos.ToList())
                {
                    logService.Write($" Transaction time:{transaction.TransactionTime}  Vehicle ID: {transaction.VehicleId}  Vehicle balance: {transaction.VehicleBalance}  With draw sum: {transaction.Sum} ");
                }
                transactionInfos.Clear();
            }
            else logService.Write("There was no transactions!");

        }

        private void WithdrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var car in parking.Vehicles)
            {
                var TempWithDraw = CalculateWithDraw(car);
                parking.Balance += TempWithDraw;
                car.WithDrawBalance(TempWithDraw);
                transactionInfos.Add(new TransactionInfo(car.Id, car.Balance, CalculateWithDraw(car)));
            }
        }

        private static decimal CalculateWithDraw(Vehicle vehicle)
        {
            var WithDrawSum = Settings.VehicleWithDraw[vehicle.VehicleType];
            if (vehicle.Balance >= WithDrawSum)
            {
                return WithDrawSum;
            }
            else if (vehicle.Balance < WithDrawSum && vehicle.Balance > 0)
            {
                return (WithDrawSum - vehicle.Balance) + (WithDrawSum - vehicle.Balance) * Settings.PenaltyMultiplier;
            }
            else
            {
                return WithDrawSum * Settings.PenaltyMultiplier;
            }

        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() <= 0)
            {
                Console.WriteLine(" Not enough space! ");
                throw new InvalidOperationException(" Not enough space at the parking! ");
            }
            else if (!parking.Vehicles.Contains(vehicle))
            {
                parking.Vehicles?.Add(vehicle);
            }
            else
            {
                Console.WriteLine("Vehicle with such ID already exists!");
                throw new ArgumentException("Duplicated ID!");
            }
        }

        public void Dispose()
        {
            parking.Vehicles.Clear();
            parking.Balance = 0;
            logTimer.Dispose();
            withdrawTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfos?.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = new ReadOnlyCollection<Vehicle>(parking.Vehicles);
            return vehicles;
        }

        public bool ContaintsVehicle(string id)
        {
            if (parking.Vehicles.Any(x => x.Id == id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (parking.Vehicles.Any(x => x.Id == vehicleId))
            {
                var vehicle = parking.Vehicles.First(x => x.Id == vehicleId);
                if (vehicle.Balance < 0)
                {
                    throw new InvalidOperationException("You can not remove vehicle with negative balance!");
                }

                parking.RemoveVehicle(vehicleId);
            }
            else throw new ArgumentException($"No vehicle with ID {vehicleId}!");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (parking.Vehicles.Any(x => x.Id == vehicleId))
            {
                var vehicle = parking.Vehicles.FirstOrDefault(x => x.Id == vehicleId);
                vehicle.TopUp(sum);
            }
            else
            {
                throw new ArgumentException($"No vehicle with ID {vehicleId}!");
            }
        }

        public Vehicle GetVehicle(string id)
        {
            if (ContaintsVehicle(id))
            {
                return parking.Vehicles.First(x => x.Id == id);
            }
            else
            {
                throw new ArgumentException($"No vehicle with ID {id}!");
            }
        }
    }
}