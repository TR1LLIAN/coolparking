﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;


namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly string logFilePath;

        public LogService(string logFilePath)
        {
            this.logFilePath = logFilePath;
            if (!File.Exists(LogPath))
            {
                File.CreateText(LogPath);
            }

            StreamWriter st = new StreamWriter(logFilePath);
            st.Flush();
            st.Close();
        }

        public string LogPath => Settings.PathFile;

        public string Read()
        {
            string ReadLogInfo = string.Empty;
            try
            {
                if(!File.Exists(logFilePath))
                {
                    throw new InvalidOperationException("File not found!");
                }
                using StreamReader streamReader = new(logFilePath, true);
                ReadLogInfo = streamReader.ReadToEnd();
                streamReader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to read the file!");
                Console.WriteLine(ex.Message);
            }
            return ReadLogInfo;
        }

        public void Write(string logInfo)
        {
            try
            {
                StreamWriter streamWriter = File.AppendText(logFilePath);
                streamWriter.WriteLine(logInfo);
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to write information into the file!");
                Console.WriteLine(ex.Message);
            }
        }
    }
}