﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services 
{
    public class TimerService : ITimerService
    {
        private readonly Timer timer;

        public double Interval 
        { 
            get => timer.Interval;
            set 
            {
                if (value > 0)
                {
                    timer.Interval = value;
                }
                else throw new ArgumentException("Interval could not be negative!");
            }
        }

        public TimerService(double interval, ElapsedEventHandler eventHandler)
        {
            this.timer = new Timer();
            this.Interval = interval;
            if (eventHandler != null)
            {
                this.timer.Elapsed += eventHandler;
            }
            else throw new ArgumentException("No event handler!");

        }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            this.timer.Dispose();
        }

        public void Start()
        {
            this.timer.Start();
        }

        public void Stop()
        {
            this.timer.Stop();
        }
    }
}