﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        private readonly string transactionTime;
        private readonly string vehicleId;
        private readonly decimal vehicleBalance;
        private readonly decimal sum;

        public string TransactionTime { get => transactionTime; }
        public string VehicleId { get => vehicleId; }
        public decimal VehicleBalance { get => vehicleBalance; }
        public decimal Sum { get => sum; }

        public TransactionInfo(string _vehicleId, decimal _vehicleBalance, decimal _sum)
        {
            this.transactionTime = DateTime.Now.ToString();
            this.vehicleId = _vehicleId;
            this.vehicleBalance = _vehicleBalance;
            this.sum = _sum;
        }
    }
}