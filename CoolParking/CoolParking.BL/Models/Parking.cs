﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.


using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static readonly Lazy<Parking> lazy =
        new Lazy<Parking>(() => new Parking());

        public string Name { get; private set; }

        private Parking()
        {
            Name = Guid.NewGuid().ToString();
            Balance = Settings.InitialBalance;
            Vehicles = new List<Vehicle>(Settings.Capacity);
        }

        public static Parking GetInstance()
        {
            return lazy.Value;
        }


        public void RemoveVehicle(string id) 
        {
            _ = Vehicles.RemoveAll(x => x.Id == id);
        }

        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
    }
}