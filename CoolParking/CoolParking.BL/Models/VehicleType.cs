﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        Bus = 3,
        Motorcycle = 1,
        PassengerCar = 2,
        Truck = 4
    }
}
