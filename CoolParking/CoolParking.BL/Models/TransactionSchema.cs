﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class TransactionSchema
    {
        public string id { get; set; }
        public decimal sum { get; set; }
    }

}
