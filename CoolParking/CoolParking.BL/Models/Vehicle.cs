﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using Newtonsoft.Json;
using System;
using System.Text;
using System.Text.RegularExpressions;


namespace CoolParking.BL.Models
{
    public class Vehicle : IEquatable<Vehicle>
    {
        private readonly static string IdPattern = @"[A-Z]{2}[-][0-9]{4}[-][A-Z]{2}";

        public Vehicle(string id, VehicleType vehicleType, decimal vehicleBalance)
        {
            if(!CheckId(id))
            {
                throw new ArgumentException("ID don't match pattern XX-YYYY-XX  ( Where X <- letter and Y <- number");
            }
            else if( vehicleBalance < 0)
            {
                throw new ArgumentException("Balance could not be negative!");
            }
            Balance = vehicleBalance;
            Id = id;
            VehicleType = vehicleType;
        }

        private string id;

        [JsonProperty("id")]
        public string Id
        {
            get
            {
                return id;
            }
            private set
            {
                if (CheckId(value))
                {
                    id = value;
                }
                else
                {
                    throw new ArgumentException("Id don't match the pattern!");
                }
            }
        }

        [JsonProperty("vehicletype")]
        public VehicleType VehicleType { get; private set; }

        public static bool CheckId(string Id)
        {
            if(Id == null)
            {
                return false;
            }
            return Regex.IsMatch(Id, IdPattern);
        }

        public static bool VehicleValidator(string id, int vehicletype, decimal balance)
        {
            if(balance <= 0 || !CheckId(id) || vehicletype<=0 || vehicletype >4)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder UniqueId = new StringBuilder(10);
            Random random = new Random();
            UniqueId.Append(GeneratePrefix());
            UniqueId.Append('-');
            UniqueId.Append(GenerateCode());
            UniqueId.Append('-');
            UniqueId.Append(GeneratePrefix());
            return UniqueId.ToString();

            string GeneratePrefix()
            {
                StringBuilder prefix = new StringBuilder(2);
                prefix.Append((char)random.Next('A', 'Z'));
                prefix.Append((char)random.Next('A', 'Z'));
                return prefix.ToString();
            }

            string GenerateCode()
            {
                StringBuilder code = new StringBuilder(2);
                for (byte i = 0; i < 4; i++)
                {
                    code.Append(random.Next(0, 9));
                }
                return code.ToString();
            }

        }

       

        private decimal balance;
        [JsonProperty("balance")]
        public  decimal Balance
        {
            get
            {
                return balance;
            }
            internal set
            {
                if (value > 0) balance = value;
                else throw new ArgumentException("Balance could not be negative!");
            }
        }

        public void WithDrawBalance(decimal WithDraw)
        {
            balance -= WithDraw;
        }

        public void TopUp(decimal value)
        {
            if (value > 0)
            {
                balance += value;
            }
            else throw new ArgumentException("Top up can't be negative!");
        }

        public bool Equals(Vehicle other)
        {
            if (this.Id == other.Id)
            {
                return true;
            }
            else return false;
        }

    }
}