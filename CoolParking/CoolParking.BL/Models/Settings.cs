﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.


using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {

        public const int Capacity = 10;

        public const float WithDrawInterval = 5000f;

        public const float LogInterval = 60000f;

        public static readonly string PathFile = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public static decimal InitialBalance { get; internal set; } = 0;

        public static readonly decimal PenaltyMultiplier = 2.5m;

        public static readonly Dictionary<VehicleType, decimal> VehicleWithDraw = new()
        {
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1m },
            { VehicleType.PassengerCar, 2m },
            { VehicleType.Truck, 5m }
        };

    }
}
