﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService parkingService = Startup.parkingService;

        [Route("balance")]
        [HttpGet]
        public IActionResult GetBalance()
        {
            return Ok(parkingService.GetBalance());
        }

        [Route("Capacity")]
        [HttpGet]
        public IActionResult GetCapacity()
        {
            return Ok(parkingService.GetCapacity());
        }

        [Route("freePlaces")]
        [HttpGet]
        public IActionResult GetFreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }


    }
}
