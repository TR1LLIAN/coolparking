﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService = Startup.parkingService;

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(parkingService.GetVehicles());
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult GetVehicleById(string id)
        {
            if (!Vehicle.CheckId(id))
            {
                return BadRequest("Not valid id(must match pattern XX - YYYY - XX X <- Uppercase letter and Y <-digit");
            }
            else if (!parkingService.ContaintsVehicle(id))
            {
                return NotFound("There is no such vehicle at the parking!");
            }
            return Ok(parkingService.GetVehicle(id));
        }


        [HttpPost]
        public ActionResult<VehicleSchema> Post([FromBody]string JsonVehicle)
        {
            VehicleSchema vehicle;
            try
            {

                vehicle = JsonConvert.DeserializeObject<VehicleSchema>(JsonVehicle);
            }
            catch (Exception)
            {
                return BadRequest("Not valid body!");
            }
            if (Vehicle.VehicleValidator(vehicle.Id, vehicle.VehicleType, vehicle.Balance))
            {
                Vehicle vh = new Vehicle(vehicle.Id, (VehicleType)vehicle.VehicleType, vehicle.Balance);

                try
                {
                    parkingService.AddVehicle(vh);
                    return Created("post", vehicle);
                }
                catch(Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return BadRequest("Not valid id (must match pattern XX-YYYY-XX X <-Uppercase letter and Y <- digit) or vehicle type(must be from 1 up to 4), or balance is negative");
            }
        }

        [Route("{id}")]
        [HttpDelete]
        public ActionResult Delete(string id)
        {
            if (!Vehicle.CheckId(id))
            {
                return BadRequest("Not valid id(must match pattern XX - YYYY - XX X < -Uppercase letter and Y < -digit)");
            }
            if (!parkingService.ContaintsVehicle(id))
            {
                return NotFound($"Vehicle with Id {id} not found! ");
            }
            else
            {
                try
                {
                    parkingService.RemoveVehicle(id);
                }
                catch(Exception ex)
                {
                    return BadRequest(ex.Message);
                }
                return NoContent();
            }
        }



    }
}
