﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService = Startup.parkingService;


        [Route("last")]
        [HttpGet]
        public ActionResult GetLast()
        {
            var temp = parkingService.GetLastParkingTransactions();
            List<TransactionsSchema> transactionSchemas = new List<TransactionsSchema>();
            foreach(var a in temp)
            {
                TransactionsSchema transactionSchema = new TransactionsSchema();
                transactionSchema.Sum = a.Sum;
                transactionSchema.TransactionTime = a.TransactionTime;
                transactionSchema.VehicleId = a.VehicleId;
                transactionSchemas.Add(transactionSchema);
            }
            return Ok(JsonConvert.SerializeObject(transactionSchemas));
        }

        [Route("all")]
        [HttpGet]
        public ActionResult GetAll()
        {
            return Ok(parkingService.ReadFromLog());
        }

        [Route("topUpVehicle")]
        [HttpPut]
        public ActionResult<TransactionSchema> Put([FromBody]string jsonTransaction)
        {
            TransactionSchema transactionSchema;
            try
            {
                transactionSchema = JsonConvert.DeserializeObject<TransactionSchema>(jsonTransaction);
            }
            catch (Exception)
            {
                return BadRequest("Not valid body!");
            }
            if (!Vehicle.CheckId(transactionSchema.id))
            {
                return BadRequest("Not valid id(must match pattern XX - YYYY - XX X <- Uppercase letter and Y <- digit)");
            }
            else if (transactionSchema.sum < 0)
            {
                return BadRequest("Sum could not be negative!");
            }
            else if(!parkingService.ContaintsVehicle(transactionSchema.id))
            {
                return BadRequest($"There is no vehicle with {transactionSchema.id} at the parking!");
            }
            else
            {
                parkingService.TopUpVehicle(transactionSchema.id, transactionSchema.sum);
                var vehicle = parkingService.GetVehicle(transactionSchema.id);
                VehicleSchema vehicleSchema = new VehicleSchema();
                vehicleSchema.Id = vehicle.Id;
                vehicleSchema.Balance = vehicle.Balance;
                vehicleSchema.VehicleType =(int) vehicle.VehicleType;

                return Ok(vehicleSchema);
            }
        }
    }
}
