﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace CoolParkingConsoleClient
{
    public class Menu
    {
        private int LenColTime = 18;
        private readonly int LenColId = 12;
        private readonly int LenColType = 14;
        const string Alias = "https://localhost:44364/";
        private static int LenColNumber = 16;
        static string WebAPiUrlParking = Alias + "api/parking/";
        static string WebAPiUrlVehicles = Alias + "api/vehicles";
        static string WebAPiUrlTransactions = Alias + "api/transactions/";
        private static HttpClient client = new HttpClient();

        readonly string MainMenu = "Select menu item from 1 to 10 \n " +
            "1) Show Parking Balance \n " +
            "2) Amount of earned money for current period \n " +
            "3) Show quantity of free parking places \n " +
            "4) Show all transactions for current period of time \n " +
            "5) Show all transactions (Read Log) \n " +
            "6) Show all vehicles at parking \n " +
            "7) Place vehicle on to the parking \n " +
            "8) Remove vehicle from the parking \n " +
            "9) Top up balance of the vehicle \n" +
            "10) Exit";

        private static void Error(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Error! {error}");
            Console.ResetColor();
        }

        public async Task ShowMenu()
        {
            int Key = 0;
            Console.ResetColor();
            while (true)
            {
                Console.WriteLine(MainMenu);
                try
                {
                    Key = int.Parse(Console.ReadLine());
                    if (Key > 10 || Key < 1)
                    {
                        throw new ArgumentException("Wrong entry!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("No such item! Please select menu item from 1 to 10!");
                    Console.ResetColor();
                }

                switch (Key)
                {
                    case 1:
                        await ShowBalance();
                        break;
                    case 2:
                        await EarnedMoneyPerPeriod();
                        break;
                    case 3:
                        await ShowFreePlace();
                        break;
                    case 4:
                        await ShowLastTransactions();
                        break;
                    case 5:
                        await ReadLog();
                        break;
                    case 6:
                        await ShowParkedVehicles();
                        break;
                    case 7:
                        await AddVehicle();
                        break;
                    case 8:
                        await RemoveVehicle();
                        break;
                    case 9:
                        await TopUpVehicle();
                        break;
                    case 10: return;
                }
            }
        }

        private async Task RemoveVehicle()
        {

            Console.Clear();
            await ShowParkedVehicles();
            string ID;
            while (true)
            {
                Console.Write("Enter vehicle id to delete it(or enter 0 to exit to main menu): ");
                try
                {
                    ID = Console.ReadLine();
                    if (ID == "0")
                    {
                        Console.Clear();
                        return;
                    }
                    else if (!Vehicle.CheckId(ID))
                    {
                        throw new ArgumentException("ID don't match pattern XX-YYYY-XX  ( Where X <- letter and Y <- number");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }



            try
            {

                Console.Clear();
                AnswerConsole();
                HttpResponseMessage request = await client.DeleteAsync(WebAPiUrlVehicles + '/' + ID);
                if (request.StatusCode == HttpStatusCode.NoContent)
                {
                    ShowBorder($" Vehicle {ID} successfully removed ");
                }
                else
                {
                    ShowBorder($" Request result {request.StatusCode}[ {await request.Content.ReadAsStringAsync()}]");
                }
            }
            catch (Exception ex)
            {
                Error(ex.Message);
            }

            Console.ResetColor();
        }

        private  async Task<VehicleSchema> TopUpVehicle()
        {
            Console.Clear();
            await ShowParkedVehicles();
            string VehicleId;
            while (true)
            {
                Console.Write("Enter vehicle id to top up it: ");
                try
                {
                    VehicleId = Console.ReadLine();
                    if (!Vehicle.CheckId(VehicleId))
                    {
                        throw new ArgumentException("ID don't match pattern XX-YYYY-XX  ( Where X <- letter and Y <- number");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            decimal TopUp;
            while (true)
            {
                Console.Write("Enter amount of top up: ");
                try
                {
                    TopUp = decimal.Parse(Console.ReadLine());
                    if (TopUp <= 0)
                    {
                        throw new ArgumentException("Top up sum can not be negative!");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }
            TopUpSchema topUpSchema = new TopUpSchema();
            topUpSchema.Id = VehicleId;
            topUpSchema.Sum = TopUp;


            Console.Clear();
            AnswerConsole();


            Console.ResetColor();
            string JsonTopUp = JsonConvert.SerializeObject(topUpSchema);
            var result = await client.PutAsJsonAsync(WebAPiUrlTransactions + "topUpVehicle", JsonTopUp);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                string TempVehicle = await result.Content.ReadAsStringAsync();
                VehicleSchema vehicle = JsonConvert.DeserializeObject<VehicleSchema>(TempVehicle);
                ShowBorder($" Successfully top up vehicle {TempVehicle} ");
                return vehicle;
            }
            else
            {
                ShowBorder($" Request result {result.StatusCode}[ {await result.Content.ReadAsStringAsync()} ] ");
                return null;
            }
        }

        static private void AnswerConsole()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void ShowBorder(string test)
        {
            Console.Clear();
            AnswerConsole();
            var temp = test;
            Console.WriteLine("╔" + new string('═', temp.Length) + "╗");
            Console.WriteLine("║" + temp + "║");
            Console.WriteLine("╚" + new string('═', temp.Length) + "╝");
            Console.ResetColor();
        }

        private static async Task ShowBalance()
        {
            HttpResponseMessage response = await client.GetAsync(WebAPiUrlParking + "balance");
            response.EnsureSuccessStatusCode();
            string balance = await response.Content.ReadAsStringAsync();
            ShowBorder($" Balance of parking is {balance} ");
        }

        private static async Task EarnedMoneyPerPeriod()
        {
            HttpResponseMessage response = await client.GetAsync(WebAPiUrlTransactions + "last");
            response.EnsureSuccessStatusCode();
            string JsonTransactions = await response.Content.ReadAsStringAsync();
            List<TransactionsSchema> transactionsSchemas = JsonConvert.DeserializeObject<List<TransactionsSchema>>(JsonTransactions);
            ShowBorder($" Money earned for current period of time: {transactionsSchemas.Sum(x => x.Sum)} ");
        }

        private static async Task ShowFreePlace()
        {
            HttpResponseMessage response = await client.GetAsync(WebAPiUrlParking + "freeplaces");
            response.EnsureSuccessStatusCode();
            string places = await response.Content.ReadAsStringAsync();
            ShowBorder($" There is {places} free places at the parking ");
        }

        private static async Task ReadLog()
        {
            int SymNum = 106;
            Console.Clear();
            AnswerConsole();

            HttpResponseMessage response = await client.GetAsync(WebAPiUrlTransactions + "all");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();

            Console.WriteLine("╔" + new string('═', SymNum) + "╗");
            Console.WriteLine("║" + new string(' ', (SymNum - 3) / 2) + "Log:" + new string(' ', (SymNum - 3) / 2) + "║");
            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            if (temp == string.Empty)
            {
                Console.WriteLine("║" + new string(' ', (SymNum - 14) / 2) + "Log is empty! " + new string(' ', (SymNum - 14) / 2) + "║");
            }
            else
            {
                Console.WriteLine(temp);
            }

            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            Console.ResetColor();
        }

        private async Task ShowLastTransactions()
        {
            HttpResponseMessage response = await client.GetAsync(WebAPiUrlTransactions + "last");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<TransactionsSchema> transactions = JsonConvert.DeserializeObject<List<TransactionsSchema>>(temp);


            Console.Clear();
            AnswerConsole();
            StringBuilder a = new StringBuilder();
            int lenColSum;
            if (transactions.Count > 0)
            {
                lenColSum = (transactions.Max(x => x.Sum).ToString().Length + 4) > 16 ? (transactions.Max(x => x.Sum).ToString().Length + 4) : 18;
                LenColTime = (transactions.Max(x => x.TransactionTime).ToString().Length + 4) > LenColTime ? (transactions.Max(x => x.TransactionTime).ToString().Length) : LenColTime;
            }
            else
            {
                lenColSum = 18;
            }
            int SymNum = lenColSum + LenColTime + LenColId + 4;
            Console.WriteLine("╔" + new string('═', SymNum) + "╗");

            a.AppendLine("║" + new string(' ', (SymNum - 11) / 2) + "Transactions" + new string(' ', (SymNum - 10 - (SymNum - 11) / 2)) + "║");
            a.AppendLine("╔" + new string('═', SymNum) + "╗");
            a.AppendLine("║" + new string(' ', (LenColTime - 17) / 2) + " Transaction date " + new string(' ', (LenColTime - 17 - (LenColTime - 18) / 2))
                + "║" + " Vehicle ID " + "║ "
                + new string(' ', (lenColSum - 16) / 2)
                + " With draw sum " + new string(' ', (lenColSum - 16 - (lenColSum - 16) / 2)) + "║");
            a.AppendLine("╚" + new string('═', SymNum) + "╝");

            if (transactions.Count != 0)
            {
                foreach (var tr in transactions)
                {
                    a.AppendLine("║" + new string(' ', (LenColTime - tr.TransactionTime.Length) / 2) + $" {tr.TransactionTime} " + new string(' ', LenColTime - tr.TransactionTime.Length - (LenColTime - tr.TransactionTime.Length) / 2)
                  + "║" + $" {tr.VehicleId} " + "║"
                  + new string(' ', (lenColSum - tr.Sum.ToString().Length - 2) / 2) + $" {tr.Sum} " + new string(' ', (lenColSum - tr.Sum.ToString().Length - 3 - (lenColSum - tr.Sum.ToString().Length - 2) / 2)) + " ║");
                }
            }
            else
            {
                a.AppendLine("║" + new string(' ', (SymNum - 36) / 2) + " There was not a single transaction!" + new string(' ', (SymNum - 36) / 2) + "║");
            }

            Console.Write(a);
            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            Console.ResetColor();
        }

        private async Task ShowParkedVehicles()
        {
            HttpResponseMessage response = await client.GetAsync(WebAPiUrlVehicles);
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<VehicleSchema> vehicles = JsonConvert.DeserializeObject<List<VehicleSchema>>(temp);


            Console.Clear();
            AnswerConsole();
            StringBuilder a = new StringBuilder();
            int LenColBalance;
            if (vehicles.Count > 0)
            {
                LenColBalance = (vehicles.Max(x => x.Balance).ToString().Length + 4) > 20 ? (vehicles.Max(x => x.Balance).ToString().Length + 4) : 20;
            }
            else
            {
                LenColBalance = 20;
            }
            int SymNum = LenColBalance + LenColId + LenColType + 1;
            a.AppendLine("║" + new string(' ', ((SymNum - 16) / 2)) + "Parked vehicles:" + new string(' ', (SymNum - 16 - ((SymNum - 16) / 2))) + "║");
            a.AppendLine('║' + new string('=', SymNum) + '║');
            a.AppendLine("╔" + new string('═', SymNum) + "╗");
            a.AppendLine("║" + " Vehicle ID " + "║"
                + new string(' ', (LenColBalance - 17) / 2) + " Vehicle balance " + new string(' ', (LenColBalance - 17) / 2)
                + "║" + " Vehicle type " + "║");
            a.AppendLine("╚" + new string('═', SymNum) + "╝");
            Console.WriteLine("╔" + new string('═', SymNum) + "╗");
            if (vehicles.Count != 0)
            {
                foreach (var Vh in vehicles)
                {
                    a.AppendLine("║" + $" {Vh.Id} " + "║"
                    + new string(' ', (LenColBalance - Vh.Balance.ToString().Length - 2) / 2) + $" {Vh.Balance} " + new string(' ', (LenColBalance - (LenColBalance - Vh.Balance.ToString().Length - 2) / 2 - Vh.Balance.ToString().Length - 3)) + "║"
                    + new string(' ', (LenColType - Vh.VehicleType.ToString().Length - 2) / 2) + $" {Vh.VehicleType} " + new string(' ', (LenColType - Vh.VehicleType.ToString().Length - 2 - (LenColType - Vh.VehicleType.ToString().Length - 2) / 2)) + "║");
                }
            }
            else
            {
                a.AppendLine("║" + new string(' ', (SymNum - 46) / 2) + " There is not a single vehicle at the parking!" + new string(' ', (SymNum - 46) / 2) + " ║");
            }

            Console.Write(a);
            Console.WriteLine("╚" + new string('═', SymNum) + "╝");
            Console.ResetColor();
        }

        private static async Task<HttpStatusCode> AddVehicle()
        {
            Console.Clear();
            string ID;
            while (true)
            {
                Console.Write("Please enter vehicle ID(remember pattern (ХХ-YYYY-XX) X-letter Y-number:");
                try
                {
                    ID = Console.ReadLine();
                    if (Vehicle.CheckId(ID))
                    {
                        break;
                    }
                    else
                    {
                        throw new ArgumentException("ID don't match the pattern!");
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }


            string VehicleTypes =
                "1 - MotorCycle \n" +
                "2 - PessengerCar \n" +
                "3 - Bus \n" +
                "4 - Truck";
            VehicleType vehicleType = VehicleType.Motorcycle;
            int Type;
            while (true)
            {
                Console.WriteLine(VehicleTypes);
                Console.Write("Please select vehicle type: ");
                try
                {
                    Type = int.Parse(Console.ReadLine());
                    if (Type < 0 || Type > 4)
                    {
                        throw new ArgumentException("Type should be from 1 to 4!");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }
            switch (Type)
            {
                case 1:
                    vehicleType = VehicleType.Motorcycle;
                    break;
                case 2:
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case 3:
                    vehicleType = VehicleType.Bus;
                    break;
                case 4:
                    vehicleType = VehicleType.Truck;
                    break;

            }

            decimal balance;

            while (true)
            {
                Console.Write("Enter vehicle balance: ");
                try
                {
                    balance = decimal.Parse(Console.ReadLine());
                    if (balance <= 0)
                    {
                        throw new ArgumentException("Balance must be positive!");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Error(ex.Message);
                }
            }

            VehicleSchema vehicle = new VehicleSchema();
            vehicle.Balance = balance;
            vehicle.Id = ID;
            vehicle.VehicleType = (int)vehicleType;
            var JsonVehicle = JsonConvert.SerializeObject(vehicle);
            var result = await client.PostAsJsonAsync(WebAPiUrlVehicles, JsonVehicle);
            var TempVehicle = await result.Content.ReadAsStringAsync();
            Console.Clear();
            ShowBorder($"Successfully Created : {TempVehicle} ");
            Console.ResetColor();
            return result.StatusCode;
        }

    }
}
